#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 10

int main()
{
    char str[N];
    printf("Scrivi la stringa:\n");
    scanf("%s", str);
    
    char sAppoggio[N];

    // E' una soluzione poco efficiente, ma di facile lettura (immagino non sia cio' che
    // il vostro professore vuole da voi)

    // Imposto due contatori su cui iterare: i avanza mentre j arretra 
    int i, j;

    //
    i = 0;
    j = strlen(str) - 1;

    while (i < N && j >= 0)
    {
        sAppoggio[i] = str[j];
        i++;
        j--;
    }
    // Ho copiato tutti i caratteri della parola al contrario, però è importante mettere
    // il terminatore alla fine della stringa affinche' si possa utilizza strcmp
    sAppoggio[strlen(str)] = '\0';

    if (strcmp(str, sAppoggio) == 0)
        printf("La stringa e' palindroma!\n");
    else
        printf("La stringa NON e' palindroma!\n");

    return 0;
}
