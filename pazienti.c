#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define K 4
#define N 2

// Non c'è nella consegna, ma mi va di metterlo
#define str_len 10

typedef struct paziente
{
    float peso;
    float altezza;
    // Non c'è nella consegna, ma mi va di metterlo
    char nome[str_len];
} paziente;

void swap(float *xp, float *yp)
{
    float temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void bubbleSort(float arr[], int n)
{
    int i, j;
    for (i = 0; i < n - 1; i++)

        // Last i elements are already in place
        for (j = 0; j < n - i - 1; j++)
            if (arr[j] > arr[j + 1])
                swap(&arr[j], &arr[j + 1]);
}

void best_bmi(paziente *input, int dim_arr, paziente *output, int dim_output)
{
    // Dichiaro due array di float che conterranno il valore BMI dei
    // pazienti
    float bmi_arr[dim_arr];
    float bmi_arr2[dim_arr];
    int i;

    for (i = 0; i < dim_arr; i++)
    {
        // Calcolo il BMI e salvo il risultato su entrambi gli array
        bmi_arr[i] = input[i].peso / (input[i].peso * input[i].peso);
        bmi_arr2[i] = bmi_arr[i];
    }

    // Questo è l'algoritmo di ordinamento più basilare e meno prestazionale
    // dell'universo, ma tornerà utile per questa soluzione naive.
    bubbleSort(bmi_arr, dim_arr);

    // Una volta ordinato solo uno dei due array, so che le prime N posizioni
    // saranno quelle di interesse, dunque scorro nuovamente entrambi gli array
    // per trovare gli indici dei pazienti dell'array di pazienti a cui 
    // corrispondono i migliori BMI.
    // Questa soluzione farebbe vomitare un algortmista, ma è semplice e funziona
    // decentemente per problemi di piccola taglia.
    int j = 0;
    for (i = 0; i < dim_output; i++)
    {
        for (j = 0; j < dim_arr; j++)
        {
            if (bmi_arr[i] == bmi_arr2[j])
                output[i] = input[j];
        }
    }

    // Il professore è un criminale, perché la consegna dice che la funzione
    // ritorna l'array. E' un tranello, non puoi ritornare un array a meno di
    // incapsularlo in una struttura dati, quindi verrebbe fuori una strutta dati
    // contenente un array di strutture dati. Quel professore è malvagio.
}

int main(int argc, char const *argv[])
{
    // Premetto che questa soluzione non è ne' efficiente ne' bella da
    // vedere, ma è la più semplice che mi e' venuta in mente
    paziente arr[K];
    int i;

    for (i = 0; i < K; i++)
    {
        printf("Indicare nome, altezza e peso del paziente n. %d:\n", i + 1);
        scanf("%s %f %f", arr[i].nome, &arr[i].altezza, &arr[i].peso);
    }

    // opzionale, stampo l'input
    printf("\nNOME\tALTEZZA\tPESO\n");
    for (i = 0; i < K; i++)
    {
        printf("%s\t%3.2f\t%3.2f\n", arr[i].nome, arr[i].altezza, arr[i].peso);
    }

    paziente migliori[N];
    best_bmi(arr, K, migliori, N);

    // opzionale, stampo l'input
    printf("\nI pazienti con BMI più basso sono:\nNOME\tALTEZZA\tPESO\n");
    for (i = 0; i < N; i++)
    {
        printf("%s\t%3.2f\t%3.2f\n", migliori[i].nome, migliori[i].altezza, migliori[i].peso);
    }

    return 0;
}
