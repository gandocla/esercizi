#include <stdio.h>
#include <stdlib.h>
// compila con -lm alla fine
// es. "gcc -g charToInt.c -lm"
#include <math.h>

#define N 10

int main(int argc, char const *argv[])
{
    char str[N];
    printf("Scrivi la stringa composta solo da numeri:\n");
    scanf("%s", str);

    int i;
    int len = 0;
    // Calcolo la lunghezza della stringa
    while (str[len] != '\0')
        len++;

    int flag = 0;
    // Effettuo il controllo sui caratteri, cioè mi assicuro che
    // il loro valore ASCII sia compreso tra 48 (cioe' '0') e 57
    // (cioe' '9').
    for (i = 0; i < len && !flag; i++)
    {
        if (str[i] < 48 || str[i] > 57)
            flag = 1;
    }

    int risultato = 0;

    if (!flag)
    {
        // La soluzione facile e semplice (e sensata) sarebbe usare
        // atoi. Ma a noi le cose semplici non piacciono (anche perché
        // con atoi potevamo evitare il controllo sui singoli char)

        // Noi sappiamo quante cifre sono grazie a len: possiamo utilizzare
        // la variabile per fare il calcolo aritmetico del risultato

        for (i = len - 1; i >= 0; i--)
        {
            // Sottraggo al valore numerico del char la posizione di 0 nella
            // tabella ASCII. poi moltiplico la cifra per 10 alla potenza della
            // posizione della cifra nel numero (come farei per la notazione
            // scientifica)(il -1 c'e' perché stiamo ragionando in termini di
            // posizioni nell'array)
            risultato += ((str[i] - 48) * pow(10, len - i - 1));
        }
        printf("La stringa è '%s'\nIl valore numerico è: %d\n", str, risultato);
    }
    else
    {
        printf("Input non valido!\n");
    }

    return 0;
}
