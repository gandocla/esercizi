#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 40

int pop(char *stack, int *i)
{
    stack[*i] = '\0';
    *i -= 1;
}

int main(int argc, char const *argv[])
{
    // Il modo con cui risolverei questo problema è utilizzare uno stack
    // (ossia una 'pila' in italiano), in modo tale da studiare le varie
    // parentesi dando priorità all'ultima in ingresso.

    char str[N];
    printf("Scrivi la stringa composta solo da '{', '}' , '[', ']', '(', ')':\n");
    scanf("%s", str);

    int i, j, len;

    i = 0;
    j = 0;
    len = 0;

    // Scorro la lista e conto i caratteri fino al terminatore per
    // ottenere la lunghezza della stringa
    while (str[len] != '\0')
        len++;

    char stack[N];

    // L'idea è questa:
    // - è la prima iterazione? Copio il contenuto della prima cella e basta
    // - ho appena aggiunta una parentesi: guarda cosa c'è nella cella antecedente
    //      - se si tratta dello stesso tipo di parentesi estraggo ("pop")
    //        entrambe le parentesi
    //      - altrimenti passo oltre
    // - il ciclo termina ogni volta che il contatore dedicato a str raggiunge la
    //   dimensione di str
    // - pop decrementa il valore del contatore dedicato allo stack (ATTENZIONE:
    //   per le operazioni con valori ottenuti via puntatori non si può fare *i--
    
    while (j < len)
    {
        stack[i] = str[j];
        if (i > 0)
        {
            switch (stack[i - 1])
            {
            case '(':
                if (stack[i] == ')')
                {
                    pop(stack, &i);
                    pop(stack, &i);
                }
                break;
            case '[':
                if (stack[i] == ']')
                {
                    pop(stack, &i);
                    pop(stack, &i);
                }
                break;
            case '{':
                if (stack[i] == '}')
                {
                    pop(stack, &i);
                    pop(stack, &i);
                }
                break;
            default:
                break;
            }
        }
        i++;
        j++;
    }

    // Potrei fare la verifica su i == 0, ma è un po' bruttino.
    // Voglio far vedere che lo stack e' vuoto.
    int dim_stack = 0;
    while (stack[dim_stack] != '\0')
        dim_stack++;

    if (dim_stack)
    {
        printf("La stringa NON e' ben formata!\n");
        printf("Stringa: %s\n", stack);
    }
    else
        printf("La stringa e' ben formata!\n");

    return 0;
}

//(()[]{})[{{}}()]
//[{{}}()]
