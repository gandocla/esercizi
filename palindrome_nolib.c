#include <stdio.h>
#include <stdlib.h>

#define N 10

int main()
{
    char str[N];
    printf("Scrivi la stringa:\n");
    scanf("%s", str);

    // Questa è la soluzione che non fa uso di string.h. Ottimo allenamento,
    // molto poco probabile nella vita vera

    // Imposto due contatori su cui iterare: i avanza mentre j arretra
    // per praticità tengo traccia della lunghezza della stringa
    int i, j, len;

    i = 0;
    j = 0;
    len = 0;

    // Scorro la lista e conto i caratteri fino al terminatore
    while (str[len] != '\0')
        len++;

    // tolgo 1 perche' gli array partono da 0
    j = len - 1;

    int flag = 0;

    // quando il primo contatore supera il secondo senza mai trovare una
    // lettera differente, allora so che la parola è palindroma.
    // Funziona sia per parole con numero di lettere dispari, sia pari.
    // Se trovo una occorrenza di non uguaglianza, allora do un valore
    // alla variabile flag, che mi fa uscire dal ciclo.
    while (i < j && !flag)
    {
        if (str[i] != str[j])
            flag = 1;
        i++;
        j--;
    }

    // Se la variabile flag non è mai stata aggiornata, allora la parola
    // è palindroma
    if (!flag)
        printf("La stringa e' palindroma!\n");
    else
        printf("La stringa NON e' palindroma!\n");

    return 0;
}
